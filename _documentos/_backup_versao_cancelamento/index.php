<?php

$h1      	 = "Home";
$title    	 = "Perfis de borracha";
    $description = "Pagina principal do site"; // Manter entre 130 a 160 caracteres
    $keywords    = $title;
    $meta_img    = "";

    include "includes/quality/class.quality.php"; 
    include "includes/_parametros.php";
    include "includes/quality/head.quality.php";
    
    $quality->compressCSS(array(

        "galeria-fotos",
        "tools/fancybox",
        "home",
        "tools/slick" 
    ));
    
    ?>
</head>
<body>

   <script src="https://kit.fontawesome.com/0cdcf9b1a2.js" crossorigin="anonymous"></script>
   <?php include "includes/_header.php"; ?>

   <main class="main-content" id="home-sect">

    <div class="banner-home">
        <div class="autoplay">

            <div id="main">
              <img src="imagens/slide1.jpg" alt="Slide-foto-1" title="Slide-foto-1" class="img-responsive">
              <div class="slide-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <h2>Perfis de Borracha</h2>
                            <p>Trabalhamos com toda a linha de perfis</p>
                            <a href="<?php echo $url?>produtos#perfis">Ver Mais</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <img src="imagens/slide2.jpg" alt="Teste_1" class="img-responsive">
            <div class="slide-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <h2>Linha Completa</h2>
                            <p>Conheça toda nossa cartela de produtos</p>
                            <a href="<?php echo $url?>produtos#catalogo">Ver Mais</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div>
            <img src="imagens/slide3.jpg" alt="Teste_1" class="img-responsive">
            <div class="slide-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <h2>Pisos</h2>
                            <p>Diversos modelos e tamanhos</p>
                            <a href="<?php echo $url?>produtos#pisos">Ver mais</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="banner-icons">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 text-justify">
                <div class="icons-content">
                  <i class="fas fa-thumbs-up"></i>
                  <h4>Produtos de Qualidade</h4>
                  <p>Trabalhamos com os melhores produtos do mercado atendendo as mais rígidas normas do segmento</p>
              </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="icons-content text-justify">
                <i class="fas fa-comments"></i>
                <h4>Atendimento Diferenciado</h4>
                <p>Prezamos sempre pelo bom relacionamento com nossos amigos e clientes, sempre procurando auxiliar em todas as necessidades dos mesmos</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="icons-content text-justify">
              <i class="fas fa-users"></i>
              <h4>Relação Pós Venda</h4>
              <p>Nossa equipe de suporte está sempre à postos para atender qualquer eventualidade que possa ocorrer</p>
          </div>
      </div>
      <div class="col-md-6 col-sm-12">
        <div class="icons-content text-justify">
            <i class="fas fa-truck"></i>
            <h4>Entrega Rápida</h4>
            <p>Procuramos sempre atender as expectativas dos nossos clientes, buscando sempre o meio mais rápido e eficiente para a entrega dos produtos</p>
        </div>
    </div>
</div>  
</div>
</div>

<div class="container">
    <div class="row" >
        <div class="col-md-12 col-xs-12 col-sm-12 text-center">
            <div class="procuts-sesh" id="produtos-sect">
                <h2>Nossos produtos</h2>
                <p>Trabalhamos com uma repleta linha de produtos de borracha</p>
                <div class="buttons-products">
                    <a id="all" href="<?php echo $url?>produtos">Ver Mais</a>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="img-grid">
    <div id="img-1" class="grid-img img-responsive"><a href="<?php echo $url?>square"> <img src="imagens/product1.jpg" alt="Square" title="Square"> </a></div>
    <div id="img-2" class="grid-img img-responsive"><a href="<?php echo $url?>produtos"><img src="imagens/product2.jpg" alt="Tipo U" title="Tipo U"></a></div>
    <div id="img-3" class="grid-img img-responsive"><a href="<?php echo $url?>lajota"><img src="imagens/product3.jpg" alt="Produto" title="Lajota"></a></div>
    <div id="img-4" class="grid-img img-responsive"><a href="<?php echo $url?>silicone"><img src="imagens/product4.jpg" alt="Cordão" title="Silicone"></a></div>
    <div id="img-5" class="grid-img img-responsive"><a href="<?php echo $url?>cordao"><img src="imagens/product5.jpg" alt="Cordão" title="Cordão"></a></div>
    <div id="img-6" class="grid-img img-responsive"><a href="<?php echo $url?>square-color"><img src="imagens/product6.jpg" alt="Square-color" title="Square-color"></a></div>
    <div id="img-7" class="grid-img img-responsive"><a href="<?php echo $url?>produtos"><img src="imagens/product7.jpg" alt="Produto"></a></div>
    <div id="img-8" class="grid-img img-responsive"><a href="<?php echo $url ?>square-fit"><img src="imagens/product8.jpg" alt="Produto"></a></div>
</div>

<div class="empresa-sect" id="empresa-sect">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 text-left">
                <img src="imagens/logo2134.png" class="img-responsive" alt="logo-3d-vedacoes">
            </div>
            <div class="col-md-6 col-sm-12 text-left">
                <div class="text-empresa">
                    <h3>SOBRE NÓS</h3>
                    <span>3D Pisos e Vedações</span>
                    <p>A 3D é uma empresa que atua no segmento de artefatos de borracha, trabalhamos com toda a linha de perfis de borracha para vedação, peças especiais e toda a linha de pisos emborrachados para playground e academias. Nossa prioridade é oferecer todo suporte necessário junto aos nossos clientes, disponibilizando de nossos materiais de excelente qualidade e preços competitivos.</p>
                    <a href="<?php echo $url?>empresa">Saiba Mais</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="produtos2">
    <h2>Pisos de Borracha</h2>

    <div class="produto-grid"><a>

        <img src="imagens/foto-1-1.jpg" class="img-responsive" alt="borracha1">
        <div class="img-text">
            <div class="container">
            <h4>Linha Fitness</h4>
            <p>Proteção e Desempenho</p>
        </div>
        </div>
    </a>

    <div class="produto-grid2"><a>
        <img src="imagens/foto-5.jpg" class="img-responsive" alt="borracha2">
        <div class="img-text">
            <div class="container">
            <h4>Linha Play</h4>
            <p>Segurança e qualidade</p>
        </div>
        </div>
    </a>
</div>
</div>
</div>

<div class="container">
    <div class="atuacao">
        <div class="text-atuacao text-center">
            <h3>LINHAS DE ATUAÇÃO</h3>
            <p>Conheça todas as linhas de atuação que atendemos</p>
        </div>    

        <div class="row">
            <div class="col-md-4">
                <div class="boxleft">
                    <i class="fas fa-quote-left"></i>
                    <p>Atendemos todas as necessidades das mais diversas exigências do mercado automobilístico e aeronáutico, com produtos diferenciados e de qualidade comprovada</p>
                    <div class="mobile-center">
                        <h5>Indústria</h5>
                        <img src="imagens/boxleft.jpg"  alt="industria"><span>Automobilística e Aeronáutica</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="boxmid">
                    <i class="fas fa-quote-left"></i>
                    <p>Peças e matérias-primas especiais para serem utilizadas em plataformas e equipamentos destinados à prospecção e extração de gás natural e petróleo</p>
                    <div class="mobile-center">
                        <h5>Indústria</h5>
                        <img src="imagens/petro.jpg" alt="industria"><span>Petrolífera e Petroquímica</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="boxright">
                    <i class="fas fa-quote-left"></i>
                    <p>Peças específicas usadas para vedação de diversos equipamentos de usinas hidrelétricas e siderúrgicas com um excelente poder de vedação e proteção</p>
                    <div class="mobile-center">
                        <h5>Indústria</h5>
                        <img src="imagens/usina.jpg" alt="industria"><span>Hidrelétrica e Siderúrgica</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="empresa-sect">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-left">
             <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3665.553885628588!2d-46.6027970918598!3d-23.25931559718992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cee959870aee6d%3A0x86dddf4b76544ba6!2sR.%20Dos%20Pedrosos%2C%2060%20-%20Terra%20Preta%2C%20Mairipor%C3%A3%20-%20SP%2C%2007600-000!5e0!3m2!1spt-BR!2sbr!4v1602262857114!5m2!1spt-BR!2sbr"></iframe>
         </div>
         <div class="col-md-6 text-left">
            <div class="text-empresa2">
                <h3 class="text-center">Fale conosco</h3>
                <div class="col-md-6">
                    <form class="form-contato">
                        <div class="form-group">
                            <label>Nome: <span class="red-color">*</span></label>
                            <input name="data[Contato][nome]" type="text" class="form-control">                                    
                        </div>
                        <div class="form-group">
                            <label>E-mail: <span class="red-color">*</span></label>
                            <input name="data[Contato][email]" type="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Telefone: <span class="red-color">*</span></label>
                            <input name="data[Contato][telefone]" type="text" class="form-control mask-phone">
                        </div>
                        <div class="form-group">
                            <label>Como nos conheceu? <span class="red-color">*</span></label>
                            <select name="data[Contato][como_conheceu]" class="form-control">
                                <option value=""> -- Selecione uma Opção -- </option>
                                <option value="Busca do Google">Busca do Google</option>
                                <option value="Outros Anúncios">Outros Anúncios</option>
                                <option value="Facebook">Facebook</option>
                                <option value="Twitter">Twitter</option>
                                <option value="Indicação">Indicação</option>
                                <option value="Outros">Outros</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Mensagem: <span class="red-color">*</span></label>
                            <textarea name="data[Contato][mensagem]" class="form-control" rows="6"></textarea>
                        </div>
                        <?php if($captcha){ ?>
                            <div class="g-recaptcha" data-sitekey="<?php echo $captcha_key_client_side; ?>"></div>
                        <?php } ?>
                        <div class="text-right">
                            <button type="submit" class="btn btn-default btn-block">Enviar</button>
                        </div>
                    </form>

                </div>
                <div class="col-md-6 text-center">
                    <div class="icons-contato">
                        <br>
                        <div class="ico1">
                            <i class="far fa-envelope"></i>
                            <p><strong>contato@3dvedacoes.com.br</strong></p>
                        </div>
                        <br>
                        <div class="ico1">
                            <i class="fas fa-phone-alt"></i>
                           <p> <strong>(11) 4280-6789</strong></p>
                        </div>
                        <br>
                        <div class="ico1">
                            <i class="fab fa-whatsapp"></i>
                             <p><strong>(11) 99104-2569</strong></p>
                        </div>
                        <br>
                        <div class="ico1">
                            <i class="fas fa-map-marker-alt"></i>
                            <p><strong>Rua dos Pedrosos, 60 - Terra Preta - Mairiporã/SP</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</main>


<?php include "includes/_footer.php"; ?>

<?php $quality->compressJS(array(
 "tools/bootstrap.min",
 "tools/jquery.nivo",
 "tools/jquery.validate.min",
 "tools/jquery.mask.min",
 "tools/jquery.fancybox",
 "jquery.quality.contact",
 "tools/jquery.slick"


)); ?>

<script>
    $(function(){

        $(".item").slick();

        $(".multiple-items").slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3
        });

        $(".autoplay").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000
        });

    });
</script>

<a id="trigger-mensagem-fancybox" href="#modal-mensagem-fancybox" style="display:none"></a>
<div id="modal-mensagem-fancybox" class="text-center"></div>
<?php if($captcha){ ?>
    <script src="https://www.google.com/recaptcha/api.js"></script>
<?php } ?>
<script>
    $(function(){
        var form_identification = ".form-contato";
        $(form_identification).validate({
            errorClass: "control-label",
            validClass: "control-label",
            rules: {
                "data[Contato][nome]" : {
                    required: true
                },
                "data[Contato][email]" : {
                    required: true,
                    email: true
                },
                "data[Contato][telefone]" : {
                    required: true
                },
                "data[Contato][como_conheceu]" : {
                    required: true
                },
                "data[Contato][mensagem]" : {
                    required: true
                }
            },
            highlight: function (element){
                $(element).parents("div.form-group").addClass("has-error").removeClass("has-success");
            }, 
            unhighlight: function (element){ 
                $(element).parents("div.form-group").removeClass("has-error").addClass("has-success"); 
            },
            submitHandler: function(form){
                var dados = $(form_identification).serialize();
                $.ajax({
                    type: "POST",
                    url: "includes/dispara-email.php",
                    data: dados,
                    dataType: "json",
                    beforeSend: function(){
                        $(".btn-send").html("Aguarde...").attr("disabled", "disabled");
                        $(".form-control").prop("disabled", true);
                    },
                    success: function(data){
                        if(data.status){
                            window.location = "envia-contato";
                        }else{
                            $("#modal-mensagem-fancybox").html(data.message);
                            $("#trigger-mensagem-fancybox").fancybox().trigger("click");
                        }
                    },
                    error: function(){
                        $("#modal-mensagem-fancybox").html("<h2>Ocorreu um Erro</h2><p>Por favor, tente novamente mais tarde.</p>");
                        $("#trigger-mensagem-fancybox").fancybox().trigger("click");
                    },
                    complete: function(){
                        $(".btn-send").html("Enviar").removeAttr("disabled");
                        $(".form-control").prop("disabled", false);
                    }
                });
            }
        });
    });
</script>

<!-- GetButton.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "55 11 99104-2569", // WhatsApp number
            call_to_action: "Fale conosco", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /GetButton.io widget -->

</body>
</html>