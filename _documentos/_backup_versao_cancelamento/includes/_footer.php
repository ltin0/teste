<footer>
    <div class="container">
        <div class="row">
                <div class="col-md-12 text-center">
                    <p>3D Vedações © 2020. Todos os direitos reservados.</p>

                    <div class="footer-content">
                    <ul>
                        <li><a href="<?php echo $url?>">Home</a></li>
                        <li><a href="<?php echo $url?>contato">Contato</a></li>
                        <li><a href="<?php echo $url?>produtos">Produtos</a></li>
                        <li><a href="<?php echo $url?>empresa">Empresa</a></li>
                        <li><a href="<?php echo $url?>mapa-site">Mapa do Site</a></li>
                    </ul>
                    </div>
                </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="div-border"></div>
            <div class="row">

                <div class="col-md-12 text-center">
                    <a rel="nofollow" href="http://validator.w3.org/check?uri=<?php echo $canonical; ?>" target="_blank" title="HTML 5 - Site Desenvolvido nos padrões W3C">
                        <img src="<?php echo $url; ?>assets/img/icons/gray-selo-html5.png" alt="HTML 5 - Site Desenvolvido nos padrões W3C">
                    </a>
                    <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo $canonical; ?>" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                        <img src="<?php echo $url; ?>assets/img/icons/gray-selo-css3.png" alt="CSS 3 - Site Desenvolvido nos padrões W3C">
                    </a>
                    <img src="<?php echo $url; ?>assets/img/icons/gray-selo-1.png" alt="Selo Quality">
                    - 
                    <a href="https://www.b2best.com.br/" title="B2Best, Cotações Online" target="_blank">
                        <img src="https://www.b2best.com.br/imagens/selo-perfil-b2best-cotacoes-online.png" alt="B2Best, Cotações Online" title="B2Best, Cotações Online">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <ul class="menu-footer-mobile">
        <li><a href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>" class="mm-call" title="Ligue"></a></li>
        <li><a href="whatsapp://send?text=<?php echo $canonical; ?>" class="mm-whatsapp" title="Whats App"></a></li>
        <li><a href="mailto:<?php echo $emailContato; ?>" class="mm-email" title="E-mail"></a></li>
        <li><button type="button" class="mm-up-to-top" title="Volte ao Topo"></button></li>
    </ul>
</footer>
<?php if($_SERVER["SERVER_NAME"] != "clientesquality" && $_SERVER["SERVER_NAME"] != "localhost"){ ?>
    <!-- Código do Analytics aqui! -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-W97J22XJXE"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-W97J22XJXE');
</script>
<?php } ?>