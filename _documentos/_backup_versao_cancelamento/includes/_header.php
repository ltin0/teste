<header itemscope itemtype="http://schema.org/Organization" id="header">
    <div class="container header-container-main">
        <div class="logo">
            <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                <span itemprop="image">
                    <img src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                </span>
            </a>
        </div>


        <nav class="menu text-left">
            <ul class="menu-list">
                <li><a href="" id="home" title="Página inicial">Home</a></li>
                <li> <a href="" id="produtos" title="Produtos">Produtos</a></li>
                <li><a href="" id="empresa" title="Empresa">Empresa</a></li>
                <li><a href="<?php echo $url; ?>contato" title="Contato">Contato</a></li>
            </ul>
        </nav>
    </div>
</header>