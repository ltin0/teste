<?php

    $title       = "Empresa";
    $description = "Saiba mais sobre a empresa"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/quality/class.quality.php"; 
    include "includes/_parametros.php"; 
    include "includes/quality/head.quality.php";
    
    $quality->compressCSS(array(
        'empresa'
    ));

?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container text-left">
            <?php echo $quality->breadcrumb(array($title)); ?>
            <h1><?php echo $h1; ?></h1>
            <img src="<?php echo $url; ?>imagens/empresa.jpg" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">

            <div class="empresa-text">
            <p>A 3D é uma empresa que atua no segmento de artefatos de borracha, trabalhamos com toda a linha de perfis de borracha para vedação, peças especiais e toda a linha de pisos emborrachados para playground e academias.</p>

            <p>Nossa prioridade é oferecer todo suporte necessário junto aos nossos clientes, disponibilizando de nossos materiais de excelente qualidade e preços competitivos.</p>

            <p>Fazendo com que o nosso atendimento não venha se tornar apenas uma simples venda e sim uma parceria de longa data.</p>

            <p>Trabalhamos com perfis de borracha fabricados em diversos compostos tais como: EPDM – NEOPRENE – NITRILICA – SBR – SILICONE – VITON – BORRACHA NATURAL.</p>

            <p>Desta forma conseguimos atender diversas empresas em diversos segmentos tais como: Empresas no ramo de – Construção Civil – Siderúrgica – Hidrelétrica – Automotiva – Alimentícia – Náutica e toda a Linha Industrial.</p>

            <p>Temos total flexibilidade de atender a sua solicitação seguindo exatamente as suas medidas ou desenhos técnicos. Nossa equipe de vendas estará sempre a disposição de nossos clientes para melhor atendê-los. Venha fazer parte de nossa historia,e com certeza a nossa parceria será de grande sucesso!</p>
        </div>

        </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $quality->compressJS(array(
        
    )); ?>
    
</body>
</html>