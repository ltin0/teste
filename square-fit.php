<?php

    $title       = "Square Fit";
    $description = "Catalogo de produtos oferecidos pela empresa"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/quality/class.quality.php"; 
    include "includes/_parametros.php";
    include "includes/quality/head.quality.php";
    
    $quality->compressCSS(array(
                "tools/fancybox",
        "galeria-fotos"
        
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container text-center">
            <?php echo $quality->breadcrumb(array($title)); ?>
            <h1><?php echo $h1;?></h1>
            <p>Quando o piso de uma academia não é apropriado para realização de atividades físicas, o atleta corre sério risco de sofrer lesões, comprometendo seu corpo, desempenho e até mesmo à saúde. Apresentamos a mais completa linha de pisos de absorção de impacto do mercado nacional para uso em Academias com diversas espessuras e acabamentos, que se adequam a modalidade física praticada na Academia, como Crossfit, Funcional, Peso livre e outras.</p>
            <section>
                <?php echo $quality->listaGaleria("square-fit", 3); ?>
            </section>

             </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $quality->compressJS(array(
         "tools/jquery.fancybox"
        
    )); ?>
    
</body>
</html>