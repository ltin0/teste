<hr><form id="form-orcamento-gerasite">
    <div class="row">
        <div class="col-xs-6 col-md-6">
            <div class="form-group">
                <label>Nome: <span class="red-color">*</span></label>
                <input name="data[Orcamento][nome]" type="text" class="form-control" placeholder="Digite seu nome">
            </div>
        </div>
        <div class="col-xs-6 col-md-6">
            <div class="form-group">
                <label>Email: <span class="red-color">*</span></label>
                <input name="data[Orcamento][email]" type="email" class="form-control" placeholder="Insira seu e-mail">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-md-6">
            <div class="form-group">
                <label>Telefone: <span class="red-color">*</span></label>
                <input name="data[Orcamento][telefone]" type="text" class="form-control mask-phone">
            </div>
        </div>
        <div class="col-xs-6 col-md-6">
            <div class="form-group">
                <label>Assunto: <span class="red-color">*</span></label>
                <input name="data[Orcamento][assunto]" type="text" class="form-control input-form-assunto">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label>Mensagem: <span class="red-color">*</span></label>
        <textarea name="data[Orcamento][mensagem]" class="form-control" rows="6"></textarea>
    </div>
    <button type="submit" class="btn btn-default btn-block btn-send">Enviar</button>
</form>
<script>
$(function(){
  $("#form-orcamento-gerasite").validate({
      errorClass: "control-label",
      validClass: "control-label",
      rules: {
          "data[Orcamento][nome]" : {
              required: true
          },
          "data[Orcamento][email]" : {
              required: true,
              email: true
          },
          "data[Orcamento][telefone]" : {
              required: true
          },
          "data[Orcamento][assunto]" : {
              required: true
          },
          "data[Orcamento][mensagem]" : {
              required: true
          }
      },
      highlight: function(element){
          $(element).parents("div.form-group").addClass("has-error").removeClass("has-success");
      },
      unhighlight: function (element){
          $(element).parents("div.form-group").removeClass("has-error").addClass("has-success");
      },
      submitHandler: function(form){
          var dados = $(form).serialize();
          $.ajax({
              type: "POST",
              url: "includes/dispara-email-orcamento.php",
              data: dados,
              dataType: "json",
              beforeSend: function(){
                  $(".btn-send").html("Aguarde...").attr("disabled", "disabled");
                  $(".form-control").prop("disabled", true);
              },
              success: function(data){
                  if(data.status){
                      window.location = "envia-contato";
                  }else{
                      $("#modal-mensagem-fancybox").html(data.message);
                      $("#trigger-mensagem-fancybox").fancybox().trigger("click");
                  }
              },
              error: function(){
                  $("#modal-mensagem-fancybox").html("<h2>Ocorreu um Erro</h2><p>Por favor, tente novamente mais tarde.</p>");
                  $("#trigger-mensagem-fancybox").fancybox().trigger("click");
              },
              complete: function(){
                  $(".btn-send").html("Enviar").removeAttr("disabled");
                  $(".form-control").prop("disabled", false);
              }
          });
      }
  });
});
</script>