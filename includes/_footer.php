<footer>
    <div class="container">
        <div class="row">
                <div class="col-md-12 text-center">
                    <p>3D Vedações © 2020. Todos os direitos reservados.</p>

                    <div class="footer-content">
                    <ul>
                        <li><a href="<?php echo $url?>">Home</a></li>
                        <li><a href="<?php echo $url?>contato">Contato</a></li>
                        <li><a href="<?php echo $url?>produtos">Produtos</a></li>
                        <li><a href="<?php echo $url?>empresa">Empresa</a></li>
                        <li><a href="<?php echo $url?>mapa-site">Mapa do Site</a></li>
                    </ul>
                    </div>
                </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="div-border"></div>
            <div class="row">
            </div>
        </div>
    </div>
    <ul class="menu-footer-mobile">
        <li><a href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>" class="mm-call" title="Ligue"></a></li>
        <li><a href="whatsapp://send?text=<?php echo $canonical; ?>" class="mm-whatsapp" title="Whats App"></a></li>
        <li><a href="mailto:<?php echo $emailContato; ?>" class="mm-email" title="E-mail"></a></li>
        <li><button type="button" class="mm-up-to-top" title="Volte ao Topo"></button></li>
    </ul>
</footer>
<?php if($_SERVER["SERVER_NAME"] != "clientesquality" && $_SERVER["SERVER_NAME"] != "localhost"){ ?>
    <!-- Código do Analytics aqui! -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-W97J22XJXE"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-W97J22XJXE');
</script>
<?php } ?>