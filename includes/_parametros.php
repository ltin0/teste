<?php

    // Principais Dados do Cliente
    $nome_empresa = "3D Vedações";
    $slogan       = "Slogan_da_Empresa";
    $emailContato = "contato@3dvedacoes.com.br";

    // Parâmetros de Unidade
    $unidades = array(
        1 => array(
            "nome" => ".",
            "rua" => "Rua dos Pedrosos, 60",
            "bairro" => "Terra Preta",
            "cidade" => "Mairiporã",
            "estado" => "São Paulo",
            "uf" => "SP",
            "cep" => "07600-000",
            "latitude_longitude" => "-23.2574601,-46.6003576", // Consultar no maps.google.com
            "ddd" => "11",
            "telefone" => "4260-2044",
            "celular" => "99104-2569",
            "link_maps" => "https://goo.gl/maps/VxotgMfXyjqbYW8f7" // Incorporar link do maps.google.com
        ),
        2 => array(
            "nome" => "",
            "rua" => "",
            "bairro" => "",
            "cidade" => "",
            "estado" => "",
            "uf" => "",
            "cep" => "",
            "ddd" => "",
            "telefone" => ""
        )
    );
    
    // Parâmetros para URL
    $quality = new classQuality(array(
        // URL local
        "http://localhost/3dvedacoes.com.br/",
        // URL online
        "https://www.3dvedacoes.com.br/"
    ));
    
    // Variáveis da head.php
    $url = $quality->url;
    $canonical = $quality->canonical;
	
    // Parâmetros para Formulário de Contato
    $smtp_contato            = "mail.qualitysmi.com.br";
    $email_remetente         = "clientes@qualitysmi.com.br";
    $senha_remetente         = "clientes@quali100";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "177.85.98.119";
    // $email_remetente         = "formulario-emails@qualitysmi-clientes.com.br";
    // $senha_remetente         = "99Vbi7H1qSSL";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $quality->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $quality->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.quality.main"
    );
        
    // Listas de Palavras Chave
$palavras_chave = array(
    "Borracha de Vedação",
      "Guarnição de Borracha",
      "Borracha de Silicone",
      "Perfil de Borracha Tipo U",
      "Fabricante de Cordão de Borracha",
      "Fabricante Borracha Retangular",
      "Perfil de Borracha Tipo E",
      "Fabricante de Silicone Translucido",
      "Fabricante de Borracha EPDM",
      "Fabricante de Borracha Esponjosa",
      "Fabricante de Perfil de Silicone",
      "Perfis de Borracha para Alta Temperatura",
      "Fabricante de Borracha Maciça",
      "Guarnições de Borracha para Esquadrias",
      "Perfil de Borracha Tipo Trapezoidal",
      "Fábrica de Vedação de Comporta",
      "Vedações para Portas de Estufas",
      "Fabricante de Vedação para Portas",
      "Borracha para Sistema Fotovoltaico",
      "Perfil de Borracha para Pré Moldado",
      "Fabricante de Borracha Nitrílica Borracha Viton",
      "Fabricante de Borracha SBR",
      "Fabricante de Borracha de Silicone",
      "Fabricante de Perfil de Borracha",
      "Fabricante de Vedação de Borracha",
      "Guarnição de Borracha para Vidros",
      "Peças Especiais de Borracha",
      "Fabricante de Peças de Borracha",
      "Fabricante de Peças de Borracha sob Medida",
      "Fabricante de Peças Especiais de Borracha",
      "Fabricante de Tira de Borracha",
      "Fabricante de Perfil de Borracha Tipo Cantoneira",
      "Fabricante de Borracha para Encaixe",
      "Perfil de Borracha Tipo Nota Musical",
      "Fabricante de Borracha EVA Auto Adesiva",
      "Borracha tipo U para Correias Transportadoras",
      "Fabricante de Tubos de Borracha",
      "Fabricante de Borracha para Corrimão",
      "Fabricante de Borracha para Vidro",
      "Perfis de Borracha Sólida",
);
   
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $quality->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $quality->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $quality->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $quality->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $quality->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $quality->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $quality->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $quality->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */