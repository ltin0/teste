/*! 
 * JsQuality v1.0.0 
 * http://qualitysmi.com.br 
 * Daniel Brito | danielb1989@gmail.com 
 */

var qualityMainJS = {
    checkSEO: function(){
        var url = window.location;
        if(url.href.indexOf("localhost/") >= 0)
        {
            var errors = [];
            var title  = $("title").html();
            var desc   = $("head meta[name=description]").attr("content");
            var canncl = $("head link[rel=canonical]").attr("href");
            var imgs = $("img");
            var mensagem_t = "Ops! :(";
            var html_erros = "";
            var s_errors = "width:300px;margin:0 auto;position:fixed;top:10px;right:10px;";
            s_errors += "z-index:9999999;text-align:center;border:1px solid #eee;";
            s_errors += "padding:0 10px;background-color:#fff;box-shadow:0 4px 13px 0 rgba(0,0,0,.45)";
            if(title.length > 70){errors.push("Title acima dos 70 caracteres.");}
            if(desc.length === 0){errors.push("Description vazia.");}
            if(url.href !== canncl){errors.push("Cannonical incorreta.");}
            for (var i = imgs.length - 1; i >= 0; i--)
            {
                var alt = imgs[i].alt;
                if(alt.length === 0){
                    errors.push("Imagem sem alt.");
                }
            };
            if(errors.length > 0)
            {
                html_erros += "<div style=\""+s_errors+"\">";
                html_erros += "<h2 style=\"margin:15px 0;font-size:16px\">"+mensagem_t+"</h2>";
                $.each(errors, function(i, val){
                    i++;
                    html_erros += "<p style=\"font-size:14px\">"+i+" - "+val+"</p>";
                });
                html_erros += "</div>";
                $("body").prepend(html_erros);
            }
        };
    },
    voltaTopo: function(){
        $(".mm-up-to-top").click(function(){
            $("html, body").animate({
                scrollTop: 0
            }, 600);
        });
    },

    mudaTamanhoHeader: function(){
        $(document).on("scroll", function(){
            if($(document).scrollTop() > 5) 
            {
                $("#header").addClass("muda-tamanho");
            }
            else
            {
                $("#header").removeClass("muda-tamanho");
            }
        });
    },

    menuMobile: function(){
        var basehref = $("head base").attr("href");
        var nomeempre = $("head meta[name=author]").attr("content");
        var bslick = "<a href=\""+basehref+"\">"+nomeempre+"</a>";
        $(".menu-list").slicknav({
            brand: bslick,
            label: "MENU",
            prependTo: "header",
            removeClasses: true
        });
    },
    scrollThis: function(){
        $(document).ready(function(){
            $('#empresa').click(function(){
                $('html, body').animate({
                    scrollTop : $('#empresa-sect').offset().top}, 'slow');
                event.preventDefault();

            })
        })
    },
    scrollThis2: function(){
        $(document).ready(function(){
            $('#produtos').click(function(){
                $('html, body').animate({
                    scrollTop : $('#produtos-sect').offset().top}, 'slow');
                event.preventDefault();

            })
        })
    },

        scrollThis3: function(){
        $(document).ready(function(){
            $('#home').click(function(){
                $('html, body').animate({
                    scrollTop : $('#home-sect').offset().top}, 'slow');
                event.preventDefault();

            })
        })
    },

    Teste: function(){
        $("#pisos").click(function(){

            $(".img-grid").addClass("teste-animation");
            event.preventDefault();
        });
    },

    geralFormularios: function(){
        var a = function(a) {
            return 11 === a.replace(/\D/g, "").length ? "(00) 00000-0000" : "(00) 0000-00009";
        },
        b = {onKeyPress: function(b, c, d, e){
                d.mask(a.apply({}, arguments), e);
            },
            placeholder: "(__) ____-____"
        };
        $(".mask-phone").mask(a, b), $.extend($.validator.messages, {
            required: "Este campo é obrigatório.",
            email: "Por favor, insira um endereço de email válido.",
            minlength: "Por favor, digite pelo menos {0} caracteres."
        });
    },
    activeSidebar: function(){
    var url = window.location;
    $('.sidebar-main ul li a[href="'+url+'"]').addClass("active-link-sidebar");
    $('#menu a[href="'+url+'"]').addClass("active-link-menu");
    $(".menu > .menu-list > li > a").click(function(){
        $(".menu > .menu-list > li > a").removeClass();
        $(this).addClass("active-link-menu");
    });
}
};


$(function(){
    qualityMainJS.scrollThis();
    qualityMainJS.scrollThis2();
    qualityMainJS.scrollThis3();
    qualityMainJS.mudaTamanhoHeader();
    qualityMainJS.Teste();
    qualityMainJS.checkSEO();
    qualityMainJS.voltaTopo();
    qualityMainJS.menuMobile();
    qualityMainJS.activeSidebar();
});