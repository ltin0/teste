<?php
    $title       = "Fabricante Borracha Retangular";
    $description = "Uma borracha retangular esponjosa é ideal para ser utilizada em um acabamento de produtos, bem como máquinas, vedação para portas e janelas.";
    $h1          = $title;
    $keywords    = 'Borracha, Borracha Retangular, Fornecedor de Borracha Retangular, Borracha Retangular em sp, industria de Borracha Retangular, fabrica de Borracha Retangular, empresa de Borracha Retangular, fornecedor de Borracha Retangular, comprar Borracha Retangular, venda de Borracha Retangular, Borracha Retangular industrial, Borracha Retangular com melhor preço';
    $meta_img    = "";

    include "includes/quality/class.quality.php";
    include "includes/_parametros.php";
    include "includes/quality/head.quality.php";

    $url_title   = $quality->formatStringToURL($title);

    $quality->compressCSS(array(
        "tools/fancybox",
        "default_qsmi/redes-sociais",
        "default_qsmi/direitos-texto",
        "default_qsmi/regioes",
        "default_qsmi/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $quality->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $quality->listaGaleria($h1, 0); ?>
                    <a href="<?php echo $url."imagens/".$url_title."/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/".$url_title."/".$url_title."-thumb.jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Uma borracha retangular esponjosa é ideal para ser utilizada em um acabamento de produtos, bem como máquinas, vedação para portas e janelas, para diminuir os impactos e também a vibração das máquinas. Por isso, é importante adquirir o serviço de um fabricante borracha retangular, que garante resultados excelentes no mercado e em todo o ramo industrial.</p>
                    
                    <h2>Onde obter o serviço dessa fabricante?</h2>
                    
                    <p>Caso você esteja em busca de um fabricante borracha retangular, entre em contato hoje mesmo com a empresa 3D Vedações, que busca trazer as melhores opções no mercado e trazer também muito mais satisfação na hora de realizar as suas atividades diárias. Portanto, conheça hoje mesmo a nossa empresa e tenha resultados excelentes e muito mais produtividade.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $quality->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>