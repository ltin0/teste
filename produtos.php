<?php

    $title       = "Produtos";
    $description = "Catalogo de produtos oferecidos pela empresa"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/quality/class.quality.php"; 
    include "includes/_parametros.php";
    include "includes/quality/head.quality.php";
    
    $quality->compressCSS(array(
                "tools/fancybox",
        "galeria-fotos"
        
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container text-center">
            <?php echo $quality->breadcrumb(array($title)); ?>
            <h1><?php echo $h1;?></h1>
            <section>
                <h2>Tudo</h2>
                <?php echo $quality->listaGaleria("tudo", 8); ?>
            </section>
                        <section id="pisos">
                <h2>Pisos</h2>
                <?php echo $quality->listaGaleria("pisos", 4); ?>
            </section>
                        <section id="perfis">
                <h2>Perfis</h2>
                <?php echo $quality->listaGaleria("perfis", 4); ?>
            </section>
            <section id="catalogo">
            <h2>Catálogo</h2>
            <img src="imagens/catalogo1.png" alt="Catalogo1" class="img-responsive">
            <img src="imagens/catalogo2.png" alt="Catalogo1" class="img-responsive">
        </section>
    </section>
    </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $quality->compressJS(array(
         "tools/jquery.fancybox"
        
    )); ?>
    
</body>
</html>