<?php

    $title       = "Square color";
    $description = "Catalogo de produtos oferecidos pela empresa"; // Manter entre 130 a 160 caracteres
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/quality/class.quality.php"; 
    include "includes/_parametros.php";
    include "includes/quality/head.quality.php";
    
    $quality->compressCSS(array(
                "tools/fancybox",
        "galeria-fotos"
        
    ));
    
?>
</head>
<body>
    
    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">
        <section class="container text-center">
            <?php echo $quality->breadcrumb(array($title)); ?>
            <h1><?php echo $h1;?></h1>
            <section>
                <?php echo $quality->listaGaleria("square-color", 3); ?>
            </section>

             </main>
    
    <?php include "includes/_footer.php"; ?>
    
    <?php $quality->compressJS(array(
         "tools/jquery.fancybox"
        
    )); ?>
    
</body>
</html>